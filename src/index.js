$(function () {
    $.getJSON("demos.json", function (demos) {
        var i,
            demo,
            demoContainer;

        for (i = 0; i < demos.length; i++) {
            demo = demos[i];

            demoContainer = $("<div></div>", {
                "class": "col-md-4"
            });

            thumbnail = $("<div></div>", {
                "class": "thumbnail"
            });

            link = $("<a></a>", {
                href: demo.key + "/"
            });

            image = $("<img></img>", {
                src: demo.img || "default.jpg"
            });

            caption = $("<div></div>", {
                "class": "caption"
            });

            title = $("<h3></h3>");
            title.text(demo.name);

            descriptionContainer = $("<div></div>", {
                "class": "description"
            });

            description = $("<p></p>");
            description.text(demo.description);

            descriptionContainer.append(description);

            caption.append(title);
            caption.append(descriptionContainer);

            link.append(image);
            link.append(caption);

            thumbnail.append(link);

            demoContainer.append(thumbnail);

            $("#content").append(demoContainer);
        }
    });
});
