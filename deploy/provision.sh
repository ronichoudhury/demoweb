# Log in as the appropriate user and run this script as root.

# Capture some useful values.
user=$(logname)
ubuntu=$(lsb_release -a | grep Codename | awk '{print $2}')

echo Install Docker and Nginx.
echo You are logged in as ${user}
echo You are running Ubuntu ${ubuntu}

# Install the latest Docker (following instructions from
# https://docs.docker.com/engine/installation/ubuntulinux/).
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-${ubuntu} main" >/etc/apt/sources.list.d/docker.list
apt-get update
apt-get purge -y lxc-docker docker.io
apt-get install -y docker-engine nginx git </dev/null

# Make the user a member of a docker group so docker can be invoked without
# sudo.
groupadd docker
gpasswd -a ${user} docker
service docker restart

# Clone the demodock repository.
cd /home/${user}
if [ ! -e demodock ]; then
    su ${user} -c "git clone https://gitlab.kitware.com/ronichoudhury/demodock.git"
fi

# Add the demodock bin directory to the path.
export PATH=/home/${user}/demodock/bin:$PATH

# Clone the demoweb repository (if it hasn't already been).
cd /home/${user}
if [ ! -e demoweb ]; then
    su ${user} -c "git clone https://gitlab.kitware.com/ronichoudhury/demoweb.git"
fi

# Start the server.
demodock init
service nginx stop
demodock up -r demoweb/src -p 80
